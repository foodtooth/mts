#!/usr/bin/evn python
# -*- coding: UTF-8 -*-

import requests
import json

from ..mts_sdk import main
from celery import Celery


app = Celery('tasks', broker='redis://localhost')


status = {
  'Submitted': '1',
  'Transcoding': '2',
  'TranscodeSuccess': '4',
  'TranscodeFail': '100',
  'TranscodeCancelled': '100',
}


@app.task
def syncTasks2Mediaexpress():
  jobs = main.list_job()

  result = []
  for job in jobs:
    job_temp = {
      'taskId': job['JobId'],
      'updateInfo': {
        'status': status.get(job['State'], 'Others'),
        'progress': job['Percent'],
        'details': {},
      }
    }
    for key, value in job.items():
      if key not in ['State', 'Percent', 'JobId']:
        job_temp['updateInfo']['details'][key] = value

    result.append(job_temp)

  syncReq({'jobs': result})


def syncReq(data, url='https://vapi.cloudifeng.com/mzapi/updateTranscodeJob'):
  headers = {
    'ossTicket': "cf77fec1-ead9-11e7-93af-9386def86895",
    'Content-Type': "application/json",
    }

  r = requests.post(url=url,
    json=data,
    headers=headers)

  print 'response: ', r.text, 'in json:', json.dumps(r.json(), sort_keys=True, indent=4, ensure_ascii=False)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
  sender.add_periodic_task(10.0, syncTasks2Mediaexpress, name='sync every 10 seconds')


# app.conf.beat_schedule = {
#   'sync-every-10-seconds': {
#     'task': 'syncTasks2Mediaexpress',
#     'schedule': 10.0,
#     },
#   }
#
# app.conf.timezone = 'UTC'
