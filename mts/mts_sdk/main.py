#!/usr/bin/evn python
# -*- coding: UTF-8 -*-

import json
import time
import uuid
from urllib import quote
from urllib import unquote

from aliyunsdkcore import client
import oss2
from aliyunsdkmts.request.v20140618 import SubmitAnalysisJobRequest
from aliyunsdkmts.request.v20140618 import QueryAnalysisJobListRequest

from aliyunsdkmts.request.v20140618 import SubmitJobsRequest
from aliyunsdkmts.request.v20140618 import QueryJobListRequest
from aliyunsdkmts.request.v20140618 import ListJobRequest

from aliyunsdkmts.request.v20140618 import SubmitSnapshotJobRequest

mts_region = 'cn-bejing'
oss_region = 'oss-cn-beijing'

mts_endpoint = 'mts.cn-beijing.aliyuncs.com'
oss_endpoint = 'http://oss-cn-beijing.aliyuncs.com'

test_video_file_name = 'videos/test-中文.mp4'
test_watermark_file_name = 'watermarks/32x32-中文.png'

access_key_id = 'LTAIh35sEJd1nYPf'
access_key_secret = 'UFJEVDGa70emXa4hlA44qGyI1IMx0G'
pipeline_id = '42b6917f31914e87b61e434245cc4fa8'
transcode_template_id = 'c88acd303ad9fcd344bc482d70c97d17'
watermark_template_id = 'bb8200843dcd4f5095139e15ec307ee8'

input_bucket = 'mamhighres'
output_bucket = 'mamlowres'

client.region_provider.modify_point('Mts', mts_region, mts_endpoint)
clt = client.AcsClient(access_key_id, access_key_secret, mts_region)


def main():
    input_file = upload_file(test_video_file_name,
                             output_bucket,
                             'demo/video/%s-中文.mp4' % (uuid.uuid1()))
    watermark_file = upload_file(test_watermark_file_name,
                                 output_bucket,
                                 'demo/watermarks/%s-中文.png' % (uuid.uuid1()))

    transcode_job_flow(input_file, watermark_file)

    snapshot_job_flow(input_file)


def upload_file(filename, bucket, obj):
    auth = oss2.Auth(access_key_id, access_key_secret)
    bucket_ = oss2.Bucket(auth, oss_endpoint, bucket)
    bucket_.put_object_from_file(obj, filename)
    return {'Location': oss_region,
            'Bucket': bucket,
            'Object': quote(obj)
            }


def get_signed_url(filepath, bucket=output_bucket, timeout=60):
  auth = oss2.Auth(access_key_id, access_key_secret)
  bucket_ = oss2.Bucket(auth, oss_endpoint, bucket)
  return bucket_.sign_url('GET', filepath, timeout)


def transcode_job_flow(input_file, watermark_file):
    system_template_job_flow(input_file, watermark_file)

    user_custom_template_job_flow(input_file, watermark_file)


def system_template_job_flow(input_file, watermark_file):
    analysis_id = submit_analysis_job(input_file, pipeline_id)
    analysis_job = wait_analysis_job_complete(analysis_id)
    template_ids = get_support_template_ids(analysis_job)

    # 可能会有多个系统模板，这里采用推荐的第一个系统模板进行转码
    transcode_job_id = submit_transcode_job(input_file, watermark_file, template_ids[0])
    transcode_job = wait_transcode_job_complete(transcode_job_id)

    print transcode_job['Output']['OutputFile']['Object']
    print("Transcode success, the target file url is http://%s.%s.aliyuncs.com/%s" % (
            transcode_job['Output']['OutputFile']['Bucket'].encode('utf8'),
            transcode_job['Output']['OutputFile']['Location'].encode('utf8'),
            unquote(transcode_job['Output']['OutputFile']['Object'].encode('utf8'))
          )
         )


def submit_analysis_job(input_file, pipeline_id_):
    request = SubmitAnalysisJobRequest.SubmitAnalysisJobRequest()
    request.set_accept_format('json')
    request.set_Input(json.dumps(input_file))
    request.set_Priority('5')
    request.set_UserData('SubmitAnalysisJob userData')
    request.set_PipelineId(pipeline_id_)

    response = json.loads(clt.do_action(request))
    return response['AnalysisJob']['Id']


def wait_analysis_job_complete(analysis_id):
    while True:
        request = QueryAnalysisJobListRequest.QueryAnalysisJobListRequest()
        request.set_accept_format('json')
        request.set_AnalysisJobIds(analysis_id)
        response = json.loads(clt.do_action(request))
        state = (response['AnalysisJobList']['AnalysisJob'][0]['State'])
        if state != 'Success':
            if state == 'Submitted' or state == 'Analyzing':
                time.sleep(5)
            elif state == 'Fail':
                print 'AnalysisJob is failed!'
                return None
        else:
            return response['AnalysisJobList']['AnalysisJob'][0]


def get_support_template_ids(analysis_job):
    result = []

    for template in analysis_job['TemplateList']['Template']:
        result.append(template['Id'])

    return result


def submit_transcode_job(input_file, watermark_file, template_id):
    watermark_config = [{
        'InputFile': json.dumps(watermark_file),
        'WaterMarkTemplateId': watermark_template_id
    }]

    obj = 'demo/transcode/%s.mp4' % (uuid.uuid1())
    outputs = [{
        'OutputObject': quote(obj),
        'TemplateId': template_id,
        'WaterMarks': watermark_config
    }]

    request = SubmitJobsRequest.SubmitJobsRequest()
    request.set_accept_format('json')
    request.set_Input(json.dumps(input_file))
    request.set_OutputBucket(output_bucket)
    request.set_OutputLocation(oss_region)
    request.set_Outputs(json.dumps(outputs))
    request.set_PipelineId(pipeline_id)
    response = json.loads(clt.do_action(request))

    return response['JobResultList']['JobResult'][0]['Job']['JobId']


def wait_transcode_job_complete(transcode_job_id):
    while True:
        request = QueryJobListRequest.QueryJobListRequest()
        request.set_accept_format('json')
        request.set_JobIds(transcode_job_id)
        response = json.loads(clt.do_action(request))

        state = (response['JobList']['Job'][0]['State'])
        if state == 'TranscodeSuccess':
            return response['JobList']['Job'][0]
        else:
            if state == 'Submitted' or state == 'Transcoding':
                time.sleep(5)
            elif state == 'TranscodeFail':
                print 'Transcode is failed!'
                return None


def user_custom_template_job_flow_without_waiting(input_file, watermark_file):
    return submit_transcode_job(input_file, watermark_file, transcode_template_id)


def user_custom_template_job_flow(input_file, watermark_file):
    transcode_job_id = submit_transcode_job(input_file, watermark_file, transcode_template_id)
    transcode_job = wait_transcode_job_complete(transcode_job_id)

    print transcode_job['Output']['OutputFile']['Object']
    print("Transcode success, the target file url is http://%s.%s.aliyuncs.com/%s" % (
            transcode_job['Output']['OutputFile']['Bucket'].encode('utf8'),
            transcode_job['Output']['OutputFile']['Location'].encode('utf8'),
            unquote(transcode_job['Output']['OutputFile']['Object'].encode('utf8'))
           )
         )


def submit_snapshot_job_async(input_file):
    obj = 'demo/snapshots/%s.jpg' % (uuid.uuid1())
    snapshot_output = {'Location': oss_region,
            'Bucket': output_bucket,
            'Object': quote(obj),
            }
    snapshot_config = {
        'OutputFile': snapshot_output,
        'Time': 1000,
        # 'Num': 2,
    }

    request = SubmitSnapshotJobRequest.SubmitSnapshotJobRequest()
    request.set_accept_format('json')
    request.set_Input(json.dumps(input_file))
    request.set_SnapshotConfig(json.dumps(snapshot_config))

    response = json.loads(clt.do_action(request))
    return response


def submit_snapshot_job(input_file):
    obj = 'demo/snapshots/%s.jpg' % (uuid.uuid1())
    snapshot_output = {'Location': oss_region,
            'Bucket': output_bucket,
            'Object': quote(obj),
            }
    snapshot_config = {
        'OutputFile': snapshot_output,
        'Time': 1000,
    }

    request = SubmitSnapshotJobRequest.SubmitSnapshotJobRequest()
    request.set_accept_format('json')
    request.set_Input(json.dumps(input_file))
    request.set_SnapshotConfig(json.dumps(snapshot_config))

    response = json.loads(clt.do_action(request))
    return response['SnapshotJob']


def snapshot_job_flow(input_file):
    snapshot_job = submit_snapshot_job(input_file)

    print("Snapshot success, the target file url is http://%s.%s.aliyuncs.com/%s" % (
            snapshot_job['SnapshotConfig']['OutputFile']['Bucket'].encode('utf8'),
            snapshot_job['SnapshotConfig']['OutputFile']['Location'].encode('utf8'),
            unquote(snapshot_job['SnapshotConfig']['OutputFile']['Object'].encode('utf8'))
           )
         )


def list_job():
  request = ListJobRequest.ListJobRequest()
  request.set_accept_format('json')
  response = json.loads(clt.do_action_with_exception(request))
  return response['JobList']['Job']


if __name__ == '__main__':
    main()
