#!/usr/bin/evn python
# -*- coding: UTF-8 -*-

from flask import request, abort, make_response, jsonify, Flask
from mts_sdk import main
from PIL import Image
from cron import tasks as tks

import uuid
import json
import glob, os

app = Flask(__name__)

@app.route('/imageTasks', methods=['POST'])
def image_tasks():
  if request.method == 'POST':
    file_path = request.form.get('filepath', '')
    task_id = request.form.get('taskid', '')

    if not file_path or not task_id:
      abort(make_response(jsonify(result="Unsatisfied params", status='1'), 422))

    size = 640, 320

    for infile in glob.glob(file_path):
      file, ext = os.path.splitext(infile)

      data = {
        'taskId': task_id,
        'poster': file + ".thumbnail",
      }

      im = Image.open(infile)
      im.thumbnail(size, Image.ANTIALIAS)
      im.save(file + ".thumbnail", "PNG")
      # im.save("/MAMexchange/2018/1/17/1.thumbnail", "PNG")

      tks.syncReq(data, 'http://10.0.16.132/mzapi/updateImageJob')

      return jsonify({ 'status': '0', 'result': { 'thumbnail': file + ".thumbnail" } })

    return jsonify({ 'status': '1', 'result': 'file ' + file_path + ' not found' })


@app.route('/tasks', methods=['POST'])
def tasks():
  if request.method == 'POST':
    file_path = request.form.get('filepath', '')

    if not file_path:
      abort(make_response(jsonify(result="Unsatisfied params", status='1'), 422))

    input_file = main.upload_file(file_path,
        main.output_bucket,
        'demo/video/%s.mp4' % (uuid.uuid1()))

    watermark_file = main.upload_file('/home/dx/mts-demo-python/mts/mts_sdk/' + main.test_watermark_file_name,
        main.output_bucket,
        'demo/watermarks/%s.png' % (uuid.uuid1()))

    ts_taskid = main.user_custom_template_job_flow_without_waiting(input_file, watermark_file)

    snapshot_job = main.submit_snapshot_job(input_file)

    return jsonify({ 'status': '0', 'result': { 'taskId': ts_taskid, 'snapshot': snapshot_job } })


@app.route('/signedUrl', methods=['GET'])
def signed_url():
  if request.method == 'GET':
    file_path = request.args.get('filepath', default='', type=str)

    if not file_path:
      abort(make_response(jsonify(result="Unsatisfied params", status='1'), 422))

    timeout = request.args.get('timeout', default=600, type=int)

    return jsonify({ 'status': '0', 'result': main.get_signed_url(file_path, timeout=timeout) })
